# This file is generated by cargo2rulesmk.py --run --config cargo2rulesmk.json.
# Do not modify this file as changes will be overridden on upgrade.

LOCAL_DIR := $(GET_LOCAL_DIR)
MODULE := $(LOCAL_DIR)
MODULE_CRATE_NAME := byteorder
MODULE_SRCS := \
	$(LOCAL_DIR)/src/lib.rs \

MODULE_RUST_EDITION := 2018
MODULE_RUSTFLAGS += \
	--cfg 'feature="default"' \

ifeq ($(call TOBOOL,$(TRUSTY_USERSPACE)),true)

MODULE_RUSTFLAGS += \
	--cfg 'feature="std"' \

endif

include make/library.mk
